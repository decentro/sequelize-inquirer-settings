/**
 * @author Benjamin Assadsolimani
 * sequelize-inquirer-settings module based on the nconf package
 */

'use strict';

const debug = require('debug')('cli:plugins:settings');
const nconf = require('nconf');
const Plugin = require('sequelize-inquirer/lib/plugins/plugin');

class SettingsPlugin extends Plugin {
  constructor() {
    super('settings');
    this.store = null;
    this.settingsFile = null;
  }

  get(key) {
    debug(`Getting settings entry: '${key}'`);
    return this.store.get(key);
  }

  set(key, value) {
    debug(`Storing settings: ${key}=${value}`);
    return this.store.set(key, value);
  }

  /**
   * Load settings from the config file
   */
  async load() {
    debug(`Loading settings from file: ${this.settingsFile}`);
    return this.store.load();
  }

  /**
   * Save settings to config file
   */
  async save() {
    debug(`Persisting settings to file: ${this.settingsFile}`);
    return this.store.save();
  }


  /**
   * Load settings from config file on CLI start
   */
  async onStart() { return this.load(); }

  /**
   * Save settings to config to file on CLI exit
   */
  async onExit() { return this.save(); }

  parsePluginOptions(options) {
    this.settingsFile = options.file || 'cli-settings.json';
    this.defaults = options.defaults || {};
  }

  initializePlugin(cli) {
    debug(`Creating new settings store with file: ${this.settingsFile}`);
    this.store = new nconf.Provider();
    this.store.argv();
    this.store.file({ file: this.settingsFile });
    this.store.defaults(this.defaults);
  }
}

const plugin = new SettingsPlugin();
module.exports = plugin;
