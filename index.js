/**
 * Sequelize-inquirer-settings
 * A `settings` plugin for the `sequelize-inquirer` package. Adds a local settings file (JSON) that acts a simple key-value store.
 * Entrypoint
 */

'use strict';

module.exports = require('./lib/sequelize-inquirer-settings');
