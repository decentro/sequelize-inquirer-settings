# sequelize-inquirer-settings

A `settings` plugin for the `sequelize-inquirer` package. Adds a local settings file (JSON) that acts a simple key-value store.

## Register plugin

The plugin can be registered like this:

``` js
const Cli = require('sequelize-inquirer');
const settingsPlugin = require('settingsPlugin');

/* Define sequelize and cli-options... */

const cli = new Cli(sequelize, options)
cli.registerPlugin(settingsPlugin);
```